package org.iesam.adriadelossantos.test;

import static org.junit.Assert.*;

import org.iesam.adriadelossantos.Product;
import org.iesam.adriadelossantos.ProductNotFoundException;
import org.iesam.adriadelossantos.ShoppingCartAdriaDelosSantos;
import org.junit.Test;

public class ShoppingCartAdriaDelosSantosTest {

	 private ShoppingCartAdriaDelosSantos _bookCart;
	    private Product _defaultBook;

	    
		@Test
		public void compruebaVacio() {
			limpiarTest();
			prepararTest();
			_bookCart.empty();
			if(_bookCart.isEmpty()) {
				assertTrue(true);
			}
			else 
				fail("El carrito no esta buit");
		}
		
		@Test
		public void añadirProducto() {
			limpiarTest();
			prepararTest();
			Product p1 = new Product("Producte", 15.50);
			_bookCart.addItem(p1);
			if((_bookCart.getBalance() == _defaultBook.getPrice() + p1.getPrice()) && _bookCart.getItemCount() == 2) {
				assertTrue(true);
			}
			else 
				fail("No entenc aquest exercici");
		}
		
		
		@Test
		public void productoNoExistente() {
			limpiarTest();
			prepararTest();
			try {
				_bookCart.removeItem(new Product("Producte", 2));
				fail("Error: Producte Esborrat!?");
			}catch (ProductNotFoundException e){
				assertTrue(true);
			}
		}
			
		@Test
		public void quitarProducto() {
			limpiarTest();
			prepararTest();
			try {
				_bookCart.removeItem(_defaultBook);
			} catch (ProductNotFoundException e) {
				e.printStackTrace();
			}
			
			if(_bookCart.getBalance() == 0) {
				assertTrue(true);
			}
			else 
				fail("Hi ha hagut algun error esborrant el producte");
		}
	    
		
	    /**
	     * Sets up the test fixture.
	     * Called before every test case method.
	     */
	    protected void prepararTest() {

	        _bookCart = new ShoppingCartAdriaDelosSantos();

	        
	        _defaultBook = new Product("Extreme Programming", 23.95);
	        _bookCart.addItem(_defaultBook);
	    }
		
		
		/**
	     * Tears down the test fixture.
	     * Called after every test case method.
	     */
	    protected void limpiarTest() {
	        _bookCart = null;
	    }
	   



}
